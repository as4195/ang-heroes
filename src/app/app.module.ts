import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { FunnelComponent } from './funnel/funnel.component';
import { DivFunnelComponent } from './div-funnel/div-funnel.component';
import { DemoComponent } from './demo/demo.component';
import { LegendComponent } from './legend/legend.component';
import { SvgfunnelComponent } from './svgfunnel/svgfunnel.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeroesComponent,
    HeroDetailComponent,
    FunnelComponent,
    DivFunnelComponent,
    DemoComponent,
    LegendComponent,
    SvgfunnelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
