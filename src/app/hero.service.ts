import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Hero} from './hero';
import {ActivatedRoute} from '@angular/router';


@Injectable({providedIn:'root'})
export class HeroService {
  heroes: Hero[];
  heroSearch: Hero[];
  error;
  private heroesurl = 'assets/heros.json';
  constructor(private httpClient: HttpClient, private route: ActivatedRoute) {
    this.httpClient.get<Hero[]>(this.heroesurl).subscribe((data) => {this.heroes = data; }, error => this.error = error );
    console.log(this.error); }
  getHeroes(): Hero[] {
    return this.heroes;
  }

  getHero(id: number) {
    return this.heroes.find(x => x.id === id);
  }

  updateHero(hero: Hero) {
    this.heroes.find((x) => {
      if (x.id === hero.id) {
        x.name = hero.name;
      }
    });
    this.getHeroes();
  }
  deleteHero(id) {
     this.heroes = this.heroes.filter( (hero) => {
      return hero.id !== id;
    });
     this.getHeroes();
     return this.heroes;
  }
  addHero(name) {
    let id=this.heroes.length+1;
    if (name.length <= 0) {
      return false;
    } else {
      this.heroes.push({id, name});
      this.getHeroes();
      return true;
    }
  }
  searchString(val: string) {
    this.heroSearch = this.heroes.filter((hero) => {
      if (hero.name.match(new RegExp(val, 'gi'))) {
        return hero.name;
      }
    });
    return this.heroSearch;
  }
}
