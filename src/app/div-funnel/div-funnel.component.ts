import { Component, Input, OnInit } from '@angular/core';


@Component({
  selector: 'app-div-funnel',
  templateUrl: './div-funnel.component.html',
  styleUrls: ['./div-funnel.component.css'],
})
export class DivFunnelComponent implements OnInit {

  ngOnInit() {
    this.layer1 = this.data[0];
    this.layer2 = this.data[1];
    this.layer3 = this.data[2];
    this.layer4 = this.data[3];
    this.heightClaculation();
    console.log(this.layer4.type);
  }

  layer1 = {
    type: "",
    color: "",
    value: 0
  };

  layer2 = {
    type: "",
    color: "",
    value: 0
  };

  layer3 = {
    type: "",
    color: "",
    value: 0
  };

  layer4 = {
    type: "",
    color: "",
    value: 0
  };
  @Input() height = 0;
  @Input() data;

  width1:string = '0';
  width2:string = '0';
  width3:string = '0';
  width4:string = '0';

  constructor() { 
    
  }

  heightClaculation() {
    var sum = this.layer1.value + this.layer2.value + this.layer3.value + this.layer4.value;
    var width1 = (this.layer1.value/sum)*this.height;
    this.width1 = width1+"px";
    var width2 = (this.layer2.value/sum)*this.height;
    this.width2 = width2+"px";
    var width3 = (this.layer3.value/sum)*this.height;
    this.width3 = width3+"px";
    var width4 = (this.layer4.value/sum)*this.height;
    this.width4 = width4+"px";
  }

  get myStyles(): any {
    return {
        'border-width' :this.width1+' 25px 0 25px',
        'border-color': this.layer1.color+' transparent'
    };
 }
  get myStyles1(): any {
  return {
      'border-width' :this.width2+' 25px 0 25px',
      'border-color': this.layer2.color+' transparent'
   };
 }

  get myStyles2(): any {
  return {
      'border-width' :this.width3+' 25px 0 25px',
      'border-color': this.layer3.color+' transparent'
  };
}

  get myStyles3(): any {
  return {
      'border-width' :this.width4+' 25px 0 25px',
      'border-color': this.layer4.color+' transparent'
  };
 }

}
