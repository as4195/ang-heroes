import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DivFunnelComponent } from './div-funnel.component';

describe('DivFunnelComponent', () => {
  let component: DivFunnelComponent;
  let fixture: ComponentFixture<DivFunnelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DivFunnelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DivFunnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
