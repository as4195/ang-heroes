import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';


@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent {
  obj =[{
    value: 50,
    type:"lost"
  },
    {
      value: 50,
    type:"ongoing"
  },
  {
    value: 50,
    type:"booked"
  },
  {
    value: 50,
    type:"commited"
  }
]

get commited(): any {
  return {
    'width': '50px',
    'height': '0px',
    'border-color': '#ffff00 transparent',
    'border-style': 'solid',
    'border-width': '50px 25px 0 25px',
    'margin-left':'75px',
    'border-radius' : '10px',
    'margin-top': '-7px'
  };
}

}
