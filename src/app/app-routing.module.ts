import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {HeroesComponent} from './heroes/heroes.component';
import {HeroDetailComponent} from './hero-detail/hero-detail.component';
import { FunnelComponent } from './funnel/funnel.component';
import { DivFunnelComponent } from './div-funnel/div-funnel.component';
import { DemoComponent } from './demo/demo.component';
import { LegendComponent } from './legend/legend.component';
import { SvgfunnelComponent } from './svgfunnel/svgfunnel.component';


const routes: Routes = [
  {path: '', redirectTo : '/svgfunnel', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'heroes', component: HeroesComponent},
  {path: 'detail/:id', component: HeroDetailComponent},
  {path: 'funnel', component: FunnelComponent},
  {path: 'divfunnel', component: DivFunnelComponent},
  {path: 'demo', component: DemoComponent},
  {path: 'reactive', component: LegendComponent},
  {path: 'svgfunnel', component: SvgfunnelComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
