import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-svgfunnel',
  templateUrl: './svgfunnel.component.html',
  styleUrls: ['./svgfunnel.component.css']
})
export class SvgfunnelComponent implements OnInit {

  constructor() {
   }

  data =[{
    type: "Open Leads",
    color: "purple",
    value: 40
  },
  {
    type: "Converted Leads",
    color: "blue",
    value: 20
  },
  {
    type: "Opportunities",
    color: "green",
    value: 30
  },
  {
    type: "Submitted Orders",
    color: "orange",
    value: 25
  }
];

  value1: number = 0;
  value2: number = 0;
  value3: number = 0;
  value4: number = 0;
  sum: number = 0;
  visi1: boolean = false;
   visi2: boolean = false;
   visi3: boolean = false;
   visi4: boolean = false;
   thumbVisible = 0;

  ngOnInit() {
    this.value1 = this.data[0].value;
    this.value2 = this.data[1].value;
    this.value3 = this.data[2].value;
    this.value4 = this.data[3].value;
  }

  public ngAfterViewChecked(): void {
    this.reCalculate(false,false,false,false);
  }

  funnel() {
    this.sum = (this.value1+this.value2+this.value3+this.value4);
    var opp = ((this.value1+this.value2+this.value3)/this.sum)*100;
    this.thumbVisible = opp;
    var conv = ((this.value1+this.value2)/this.sum)*100;
    var open = ((this.value1)/this.sum)*100;
    document.getElementById("opportunities").setAttribute("height", opp+"%");
    document.getElementById("converted-leads").setAttribute("height", conv+"%");
    document.getElementById("open-leads").setAttribute("height", open+"%");
    if(open) {
      document.getElementById('open-leads-arrow').style.visibility = 'visible';
    document.getElementById("open-leads-arrow").setAttribute("y", open+"%");
    } else {
      document.getElementById('open-leads-arrow').style.visibility = "hidden";
    }
    if(conv) {
      document.getElementById('converted-leads-arrow').style.visibility = 'visible';
    document.getElementById("converted-leads-arrow").setAttribute("y", conv+"%");
    } else {
      document.getElementById('converted-leads-arrow').style.visibility = "hidden";
    }
    if(opp>0) {
      document.getElementById('opportunities-arrow').style.visibility = 'visible';
    document.getElementById("opportunities-arrow").setAttribute("y", opp+"%");
    } else {
      document.getElementById('opportunities-arrow').style.visibility = "hidden";
    }
  }

  get colorlegend1(): any {
    return {
      'border-color': '#61aaf2'
    }
  }
  get colorlegend2(): any {
    return {
      'border-color': '#9b73eb'
    }
  }
  
  get colorlegend3(): any {
    return {
      'border-color': '#6bce70'
    }
  }
  
  
  get colorlegend4(): any {
    return {
      'border-color': '#ffc951'
    }
  }

  reCalculate(visi1, visi2, visi3, visi4) {
    if(visi1){
      this.visi1 = !this.visi1;
    } else if(visi2){
      this.visi2 = !this.visi2;
    } else if(visi3){
      this.visi3 = !this.visi3;
    } else if(visi4){
      this.visi4 = !this.visi4;
    }
    if(!this.visi1){
      this.value1 = this.data[0].value;
    } else {
      this.value1 = 0;
    }
    if(!this.visi2){
      this.value2 = this.data[1].value;
    } else {
      this.value2 = 0;
    }
    if(!this.visi3){
      this.value3 = this.data[2].value;
    } else {
      this.value3 = 0;
    }
    if(!this.visi4){
      this.value4 = this.data[3].value;
    } else {
      this.value4 = 0;
    }
    this.funnel();
  }

}
