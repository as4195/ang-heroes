import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SvgfunnelComponent } from './svgfunnel.component';

describe('SvgfunnelComponent', () => {
  let component: SvgfunnelComponent;
  let fixture: ComponentFixture<SvgfunnelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SvgfunnelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SvgfunnelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
