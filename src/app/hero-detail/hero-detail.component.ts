import { Component, OnInit, Input } from '@angular/core';
import {HeroService} from '../hero.service';
import { ActivatedRoute } from '@angular/router';
import {Hero} from '../hero';
import {Location} from '@angular/common';


@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {
  @Input() hero: Hero;
  constructor(private heroService: HeroService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    this.getHero();
  }
  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.hero = this.heroService.getHero(id);
  }

  goback(): void {
    this.location.back();
  }

  save(): void {
    this.heroService.updateHero(this.hero);
    this.location.back();
  }
  changed(name) {
    console.log(name);
  }
}
