import { Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-legend',
  templateUrl: './legend.component.html',
  styleUrls: ['./legend.component.css'],
   
})
export class LegendComponent implements OnInit {

  ngOnInit() {
    this.reCalculate(false, false,false, false);
  }

  data =[{
    type: "lost",
    color: "purple",
    value: 40
  },
  {
    type: "ongoing",
    color: "blue",
    value: 20
  },
  {
    type: "booked",
    color: "green",
    value: 30
  },
  {
    type: "commited",
    color: "orange",
    value: 5
  }
];

   sendData = [];
   height = 300;
   visi1: boolean = false;
   visi2: boolean = false;
   visi3: boolean = false;
   visi4: boolean = false;

  constructor() { }

  get colorlegend1(): any {
    return {
      'border-color': this.data[0].color
    }
  }
  get colorlegend2(): any {
    return {
      'border-color': this.data[1].color
    }
  }
  
  get colorlegend3(): any {
    return {
      'border-color': this.data[2].color
    }
  }
  
  
  get colorlegend4(): any {
    return {
      'border-color': this.data[3].color
    }
  }

  reCalculate(visi1, visi2, visi3, visi4) {
    this.sendData = [];
    if(visi1){
      this.visi1 = !this.visi1;
    } else if(visi2){
      this.visi2 = !this.visi2;
    } else if(visi3){
      this.visi3 = !this.visi3;
    } else if(visi4){
      this.visi4 = !this.visi4;
    }
    if(!this.visi1){
      this.sendData.push(this.data[0]);
    } 
    if(!this.visi2){
      this.sendData.push(this.data[1]);
    } 
    if(!this.visi3){
      this.sendData.push(this.data[2]);
    } 
    if(!this.visi4){
      this.sendData.push(this.data[3]);
    } 
  }
}
