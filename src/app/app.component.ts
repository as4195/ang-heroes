import {Component, OnInit, Input} from '@angular/core';
import {HeroService} from './hero.service';
import { Hero } from './hero';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @Input() key;
  heroSearch: Hero[];
  titleOfPage = 'Sales Funnel' ;
  constructor(private heroservice:HeroService){}
  searchString(val:string){
    this.heroSearch=this.heroservice.searchString(val);
  }
}
