import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-funnel',
  templateUrl: './funnel.component.html',
  styleUrls: ['./funnel.component.css']
})
export class FunnelComponent implements OnInit {

  value1: number = 1;
  value2: number = 1;
  value3: number = 1;
  value4: number = 1 ;
  show: boolean = false;
  sum: number = 0;

  constructor() { }

  ngOnInit() {
  }


  funnel() {
    console.log(((this.value1/this.sum)*100));
    this.sum = this.value1+this.value2+this.value3+this.value4;
   document.documentElement.style.setProperty("--top-layer1",(((this.value1/this.sum)*100)-1)+"%"); 
   document.documentElement.style.setProperty("--top-layer2",(((this.value2/this.sum)*100)-1)+"%"); 
   document.documentElement.style.setProperty("--top-layer3",(((this.value3/this.sum)*100)-1)+"%"); 
   document.documentElement.style.setProperty("--top-layer4",(((this.value4/this.sum)*100)-1)+"%"); 
  }

}
